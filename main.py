from pdfkit import from_file
options = {
    'page-size': 'Letter',
    'encoding': "UTF-8",
    'custom-header': [
        ('Accept-Encoding', 'gzip')
    ],
    'no-outline': None
}

file_generated = from_file('./Factura.html', './example.pdf', options=options)
